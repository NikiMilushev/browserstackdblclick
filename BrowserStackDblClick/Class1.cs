﻿using NUnit.Framework;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.iOS;
using System;
using OpenQA.Selenium.Appium.Enums;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace BrowserStackDblClick
{
    [TestFixture]
    public class Class1
    {
        [Test]
        public void Test()
        {

            var appiumOptions = new AppiumOptions();

            appiumOptions.AddAdditionalCapability(MobileCapabilityType.BrowserName, "iPhone");
            appiumOptions.AddAdditionalCapability("os_version", "14");
            appiumOptions.AddAdditionalCapability("device", "iPad Pro 12.9 2020");
            appiumOptions.AddAdditionalCapability("realMobile", "true");
            appiumOptions.AddAdditionalCapability("automationName", "XCUITest");
            appiumOptions.AddAdditionalCapability("project", "BrowserStackDblClick");
            appiumOptions.AddAdditionalCapability("browserstack.user", "");
            appiumOptions.AddAdditionalCapability("browserstack.key", "");

            var driver = new IOSDriver<AppiumWebElement>(new Uri("https://hub-cloud.browserstack.com/wd/hub/"), appiumOptions);

            driver.Navigate().GoToUrl("https://yari-demos.prod.mdn.mozit.cloud/en-US/docs/Web/API/Element/dblclick_event/_samples_/Examples");

            var element = driver.FindElement(By.CssSelector("aside"));

            Console.WriteLine(element.Text);

            Actions action = new Actions(driver);
            action.MoveToElement(element);
            action.DoubleClick();
            action.Perform();

            driver.Quit();
        }
    }
}
